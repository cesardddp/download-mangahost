from selenium import webdriver
from requests import get
from os import path,environ,mkdir,system
from selenium.webdriver.chrome.options import Options

import logging
import zipfile
import sys

logging.basicConfig(filename="log.log",filemode='a'format=f"{asctime} - {} - {message}")


# url = sys.argv[1]#pega url de download do webdriver compativel
url = "https://chromedriver.storage.googleapis.com/84.0.4147.30/chromedriver_win32.zip"
headers = {'User-Agent': 'Chrome/85.0.4183.38'}#fakeheader pra evitar status 403 no request.get do site mangahost
# desktop_path = environ["HOMEPATH"] + "\\Desktop"


exist_desktop_folder = False


try:
    mkdir("mangas")
    exist_desktop_folder = True
except FileExistsError as e:
    exist_desktop_folder = True
finally:
    desktop_path = desktop_path+"\\mangas-Baixados"


download_site = get(url)
if not path.exists("temp.file"):
    with open("temp.file","wb") as ref:
        ref.write(download_site.content)
    with zipfile.ZipFile("temp.file","r") as zip_ref:
        zip_ref.extractall(".")
        with open("filename.txt",'w') as ref:
            ref.write(zip_ref.infolist()[0].filename)


manga_name = input("anime name: ")

chrome_options = Options()
chrome_options.add_argument('--headless')
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument("--window-size=1920x1080")

chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(10) # seconds

# def find_dl(webdriver,manga_name,):
chrome.get(f"https://mangahost2.com/find/{manga_name}")
chrome.find_elements_by_css_selector("h4.entry-title")[0].click()
# search = chrome.find_element_by_id("s")
# search.send_keys("bleach")#replace por imput name
# search.submit()#replace por imput name
chrome.find_elements_by_css_selector("h4.entry-title")[0].find_element_by_css_selector("a").click()
chrome.find_element_by_css_selector("div.cap").click()
chrome.find_element_by_css_selector("a.btn-green").click()
chrome.find_element_by_id("capitulos-3").find_elements()
last_page = chrome.find_element_by_id("capitulos-3").find_elements_by_tag_name("option")[-1].text 
last_page = int(last_page)

anime_tile = manga_name#chrome.title[:(chrome.title.find('-'))-1].replace(" ","_")

try:
    mkdir(desktop_path+"\\"+anime_tile)
except FileExistsError as e:
    alredy_exist_desktop_folder_manga = True
finally:
    desktop_path = desktop_path+"\\"+anime_tile

if exist_desktop_folder:#verifica se a pasta foi criada na area de trabalho, se não pass
    ...

for i in range(last_page):
    with open(desktop_path+f"\img{i+1}.png",'wb') as page:
    # element = chrome.find_element_by_id(f"img_{i+1}")
    # img_src = element.get_attribute("src")
    # content = get(img_src,headers=headers).content
        page.write(
            get(
                chrome.find_element_by_id(f"img_{i+1}")
                .get_attribute("src"),
                headers=headers)
            .content

        )
    chrome.get(chrome.current_url[:(-1 if i <=8 else -2)] + str(i+2))

system("explorer "+ desktop_path) 

# find_dl(chrome,manga_name)